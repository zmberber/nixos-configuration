# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.efiSupport = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.device = "nodev";

  networking.hostName = "kiste"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp3s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_DK.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  

  # Configure keymap in X11
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    groups = {
      # webpage = { };
      mischpoke = { };
    };
    users = {
      berber = {
        isNormalUser = true;
        extraGroups = [
          "wheel" # Enable ‘sudo’ for the user.
          "mischpoke"
        ];
        # openssh.authorizedKeys.keyFiles = [
        #   # (builtins.fetchurl { url = "https://gitlab.com/zmberber.keys"; })
        #   "/etc/nixos/ssh_pubkeys"
        # ];
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOwa7hVtr7v3G6EL2tZ+FYTUACmcLaLe0H5xsmVz+r67 berber@konpyu"
        ];
      };
      mischpoke = {
        isNormalUser = true;
        group = "mischpoke";
        # extraGroups = [
        #   "webpage"
        # ];
      };
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    bind
    emacs
    git
    wget
    jq
    hugo
    nginx
    openssh
    seafile-shared
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    startWhenNeeded = true;
    challengeResponseAuthentication = false;
  };

  # Cron
  services.cron = {
    enable = true;
    systemCronJobs = [
      ''
        HETZNER_ZONE_NAME='zmberber.com'
        HETZNER_RECORD_NAME='mischpoke'
        HETZNER_RECORD_TTL=86400

        */5 * * * * root /usr/bin/dyndns.sh &>> /var/log/dyndns
        */5 * * * * root /usr/bin/dyndns.sh -T AAAA &>> /var/log/dyndns6
      ''
    ];
  };

  # Nginx

  security.acme = {
    certs = {
      "mischpoke.zmberber.com" = {
        email = "cert@zmberber.com";
      };
    };
    email = "cert@zmberber.com";
    acceptTerms = true;
  };
  
  services.nginx = {
    enable = true;
    virtualHosts."mischpoke.zmberber.com" = {
      addSSL = true;
      enableACME = true;
      root = "/var/www/mischpoke-website/public/";
    };
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 80 443 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}
